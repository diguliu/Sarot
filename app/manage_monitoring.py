#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from core.task import *
from core.importer import *
from app.sense_temperature import *

class ManageMonitoring(Task):

    def __init__(self, sensor, actuator, node, slack_time=1e308, wcet=-1, delay = 2):
        self.sensor = sensor
        self.actuator = actuator
        self.node = node
        self.start = time.time()
        self.slack_time = slack_time
        self.wcet = wcet
        self.delay = delay

    def main_run(self):
        self.sensor.add_task(SenseTemperature(self.actuator, self.sensor))
        time.sleep(self.delay)
        self.node.add_task(ManageMonitoring(self.sensor, self.actuator, self.node))


