#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.scheduling_algorithm import *
import time

class Edf(SchedulingAlgorithm):

    def schedule(self, tasks):
        current_time = time.time()
        if len(tasks) == 0:
            return None
        return sorted(tasks, key=lambda(task): task.deadline(current_time))[0]

