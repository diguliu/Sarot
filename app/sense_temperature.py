#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.task import *
from core.importer import *
from app.temperature import *

class SenseTemperature(Task):

    def __init__(self, actuator, node, slack_time=1e308, wcet=-1):
        self.actuator = actuator
        self.node = node
        self.start = time.time()
        self.slack_time = slack_time
        self.wcet = wcet

    def main_run(self):
        temperature = System.get_instance().environment.get_attributes('temperature')
        message = Temperature(self.node.id, self.actuator.id, temperature, time.time()) 
        self.node.route_message(message)

