#!/usr/bin/env python
# -*- coding: utf-8 -*-

from node import *

class TemperatureSensor(Node):

    def __init__(self, temperature, neighbors=[], routes={}, tasks_queue=[], state=On(), visited={}):
        self.id = System.get_instance().new_id()
        self.temperature = temperature
        self.neighbors = neighbors
        self.routes = routes
        self.tasks_queue = tasks_queue
        self.state = state
        self.visited = visited
