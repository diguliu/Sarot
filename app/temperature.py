#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.message import *


class Temperature(Message):
    def __init__(self, sender, receiver, temperature, checked_on):
        self.sender = sender
        self.receiver = receiver
        self.temperature = temperature
        self.checked_on =  checked_on

    def __str__(self):
        system = System.get_instance()
        return "%s(%s) - (%s -> %s)" % (self.__class__.__name__, self.temperature, system.nodes[self.sender], system.nodes[self.receiver])
        

