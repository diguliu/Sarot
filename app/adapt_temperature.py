#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.task import *
from core.importer import *

class AdaptTemperature(Task):

    def __init__(self, temperature, checked_on, node=None, slack_time=1e308, wcet=-1):
        self.ideal_temperature = 28
        self.temperature = temperature
        self.checked_on = checked_on
        self.node = node
        self.start = time.time()
        self.slack_time = slack_time
        self.wcet = wcet

    def main_run(self):
        System.get_instance().environment.add_variations({'temperature': self.temperature_adaptation()})


    def temperature_adaptation(self):
        if self.temperature < self.ideal_temperature:
            return self.intensity()
        else:
            return -self.intensity()

    def intensity(self):
        difference = abs(self.ideal_temperature - self.temperature)
        if difference > 30:
            return 6
        elif difference > 20:
            return 4
        else:
            return 2
