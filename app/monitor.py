#!/usr/bin/env python
# -*- coding: utf-8 -*-

from base_station import *

class Monitor(BaseStation):
    
    def __init__(self, temperature_log = [], neighbors=[], routes={}, tasks_queue=[], state=On(), visited={}):
        self.id = System.get_instance().new_id()
        self.temperature_log = temperature_log
        self.neighbors = neighbors
        self.routes = routes
        self.tasks_queue = tasks_queue
        self.state = state
        self.visited = visited

