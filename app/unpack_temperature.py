#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app.temperature import *
from app.adapt_temperature import *
from core.unpack_message import *
from core.importer import *
import time

class UnpackTemperature(UnpackMessage):

    def read_message(self):
        system = System.get_instance()
        node = system.nodes[self.message.receiver]
        task = AdaptTemperature(self.message.temperature, self.message.checked_on, node)
        self.node.add_task(task)
        print "==> Unpacked AdaptTemperature task to %s." % (node)
