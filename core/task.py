#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

#abstract
class Task():

    def __init__(self, node=None, slack_time=1e308, wcet=-1):
        self.node = node
        self.start = time.time()
        self.slack_time = slack_time
        self.wcet = wcet

    def deadline(self, current_time):
        return self.start+self.slack_time-current_time

    def main_run(self):
        pass

    def alternative_run(self):
        pass
