#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.state_node import *

class Off(StateNode):

    def send_data(message,node):
        pass

    def schedule_task(tasks):
        return None

    def run_task(task):
        pass

    def off():
        return True
