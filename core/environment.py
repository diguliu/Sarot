#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import time
import thread
import inspect

class Environment():
    
    def __init__(self): 
        #dictionary that keeps the decorators of the enviroment
        #ex: self.decorators[temp1] = temp1_decorator
        self.decorators = {}
        self.delay = 7

    def simulate(self):
        while 1:
#            print "+ Starting process on environment."
            for key in self.decorators:
                variable = self.decorators[key].simulate()
                setattr(self, key, variable)
                print "==> Environment temperature:", self.temperature
            time.sleep(self.delay)
#        print "- Starting process on environment."
                
    
    #names is a list of attributes that a task whants to read from the environment
    def get_attributes(self,names):
        # I know this is pure evil...but doing this thing over here is to make
        # easier to the sensors that wants only one attribute. So instead of
        # returning a list with only one attribute in the end, it returns only
        # the attribute. If you have any more elegant way to do this, do it
        # now!  ^^
        if not isinstance(names, list):
            names = [names]

        attributes = []
        for name in names:
            if not self.decorators.has_key(name):
                klass_name = "".join([x.capitalize() for x in name.split('_')])
                decorator = globals()[klass_name+'Decorator']()
                self.decorators[name] = decorator
                variable = decorator.simulate()
                setattr(self, name, variable)

            attributes.append(getattr(self, name))
                
        if len(attributes) == 1:
            return attributes[0]
        
        return attributes
       

    #variations is a dictionary that keeps the name of the environment attribute as key
    #and keeps the value of the attribute as value
    def add_variations(self,variations):
        for variation in variations:
            decorator = self.decorators[variation]
            value = variations[variation]
            vars(decorator)[variation] += value
            setattr(self, variation, getattr(decorator, variation))


class TemperatureDecorator(Environment):
    
    def __init__(self): 
        #dictionary that keeps the decorators of the enviroment
        #ex: self.decorators[temp1] = temp1_decorator
        self.temperature = random.randint(70,75)

    def simulate(self):
        self.temperature += random.randint(2,4)
        return self.temperature
