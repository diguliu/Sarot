#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.node import *

class BaseStation(Node):
    
    instance = None

    @classmethod
    def get_instance(self):
        klass = self
        if klass.instance is None:
            klass.instance = klass()
        return klass.instance

    def flood(self, message):
        for node in System.get_instance().nodes.keys():
            self.route_message(node, message)

    def group_schedule_tasks():
        pass

    def set_node_state(node, state):
        node.set_state(state)
