class System():

    instance = None

    def __init__(self):
        self.i = 0

    @classmethod
    def get_instance(self):
        if self.instance is None:
            self.instance = self()
        return self.instance

    def new_id(self):
        self.i += 1
        return self.i
