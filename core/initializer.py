from core.base_station import *
from core.environment import *
#from core.faulty import *
#from core.message import *
from core.node import *
#from core.off import *
#from core.on import *
#from core.scheduling_algorithm import *
#from core.state_node import *
#from core.task import *
#from core.turn_off import *
#from core.turn_on import *
#from core.unpack_message import *

from app.edf import *
#from app.send_temperature import *
#from app.temperature_sensor import *

#from app.monitor import *
from app.manage_monitoring import *
#from app.temperature import *
#from app.unpack_temperature import *
#from app.rate_monotonic import *

from core.importer import *


class Initializer():

    def __init__(self):
        
        system = System.get_instance()

        system.nodes = {}
        system.unpackers = {}

        system.unpackers['Temperature'] = 'UnpackTemperature'
        
        base_station = BaseStation.get_instance()
        base_station.state.scheduling_algorithm = Edf()

        sensor = Node([base_station], {1: [{1: 1}], 3: [{1: 2}]})
        sensor.state.scheduling_algorithm = Edf()

        actuator = Node([base_station],{1: [{1: 1}], 2: [{1: 2}]})
        actuator.state.scheduling_algorithm = Edf()

        base_station.tasks_queue = [ManageMonitoring(sensor, actuator, base_station)]
        base_station.routes = {2: [{2: 1}], 3: [{3: 1}]}

        base_station.neighbors = [sensor, actuator]

        system.nodes[base_station.id] = base_station
        system.nodes[sensor.id] = sensor
        system.nodes[actuator.id] = actuator
        system.environment = Environment()
