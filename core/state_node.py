#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.scheduling_algorithm import *

class StateNode():
    def __init__(self, scheduling_algorithm=SchedulingAlgorithm()):
        self.scheduling_algorithm = scheduling_algorithm

    def send_message(self, node, message):
        pass

    def receive_message(self, node, message):
        pass

    def schedule_task(self, tasks):
        return self.scheduling_algorithm.schedule(tasks)

    def run_task(self, task):
        pass

    def off(self):
        return False
