#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.on import *
from core.importer import *
import thread
import inspect

#abstract
class Node():

    def __init__(self, neighbors=[], routes={}, tasks_queue=[], state=On(), visited={}):
        self.id = System.get_instance().new_id()
        self.neighbors = neighbors
        self.routes = routes
        self.tasks_queue = tasks_queue
        self.state = state
        self.visited = visited

    def add_route(self, node_id, route):
        if self.routes.has_key(node_id):
            self.routes[node_id].append(route)
        else:
            self.routes[node_id] = [route]
            

    def __str__(self):
        return "Node-%s" % (self.id)

    def set_state(self, state):
        self.state = state

    def find_neighbors():
        pass

    def add_task(self, task):
        self.tasks_queue.append(task)
        if len(self.tasks_queue) == 1:
            thread.start_new_thread(self.process, ())

    def generate_routes(self):
        for neighbor in self.neighbors:
            neighbor.create_route(self.id,self.id,1)

    def create_route(self, target, next_hop, cost):
        if not self.visited.has_key(target):
            self.visited[target] = {}
        if not self.visited[target].has_key(next_hop) and target != self.id:
            self.add_route(target, {next_hop: cost})

            self.visited[target][next_hop] = 1
            for neighbor in self.neighbors:
                if neighbor.id != next_hop:
                    neighbor.create_route(target,self.id,cost+1)

    def route_message(self,message):
        print "==> Routing message: " + str(message)
        if self.routes.has_key(message.receiver):
            system = System.get_instance()
            for route in self.routes[message.receiver]:
                next_hop = system.nodes[route.keys()[0]]
                if not next_hop.state.off():
                    self.state.send_message(next_hop, message)
                    break
        else:
            print "##> No routes from %s to %s" % (self, system.nodes[message.receiver])

    def receive_message(self, message):
        self.state.receive_message(self, message)

    def schedule_task(self):
        return self.state.schedule_task(self.tasks_queue)

    def process(self):
#        print "+ Starting process on %s" % (self)
        while len(self.tasks_queue):
            task = self.schedule_task()
            if task is not None:
                self.tasks_queue.remove(task)
                self.state.run_task(task)
#        print "- Stoping process on %s" % (self)

