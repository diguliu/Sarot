#!/usr/bin/env python
# -*- coding: utf-8 -*-

from task import *

#abstract
class UnpackMessage(Task):

    def __init__(self, node, message, slack_time=1e308, wcet=-1):
        self.node = node
        self.message = message
        self.start = time.time()
        self.slack_time = slack_time
        self.wcet = wcet


    def main_run(self):
        print "==> %s unpacked message: %s" % (self.node, self.message)
        if self.message.receiver == self.node.id:
            print "    -> %s decided to read message." % (self.node)
            self.read_message()
        else:
            print "    -> %s decided to route message." % (self.node)
            self.node.route_message(self.message)

    def read_message(self):
        pass
