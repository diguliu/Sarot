#!/usr/bin/env python
# -*- coding: utf-8 -*-

from state_node import *
from core.importer import *
from app.unpack_temperature import *

class On(StateNode):

    def send_message(self, node, message):
        print "==> Sending to %s message: %s" % (node, message)
        node.receive_message(message)

    def receive_message(self, node, message):
        print "==> %s receiving message: %s" % (node, message)
        node.add_task(globals()[System.get_instance().unpackers[message.__class__.__name__]](node, message))

    def run_task(self, task):
        task.main_run()
