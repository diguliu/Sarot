#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.importer import *

#abstract
class Message():
    def __init__(self, sender, receiver):
        self.sender = sender
        self.receiver = receiver

    def __str__(self):
        system = System.get_instance()
        return "%s - (%s -> %s)" % (self.__class__.__name__, system.nodes[self.sender], system.nodes[self.receiver])
