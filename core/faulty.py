#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.state_node import *

class Faulty(StateNode):

    def send_data(message,node):
        pass

    def run_task(task):
        task.alternative_run()
