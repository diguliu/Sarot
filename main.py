#!/usr/bin/env python
# -*- coding: utf-8 -*-

import core
import app
import thread
import time
from core.initializer import *

class Main():


    def __init__(self):
        print "Starting..."
        system = System.get_instance()
        Initializer()

#        for node in system.nodes.values():
#            node.find_neighbors
#            node.generate_routes()
        thread.start_new_thread(system.environment.simulate, ())

        for node in system.nodes.values():
            thread.start_new_thread(node.process, ())

        time.sleep(600)


Main()
